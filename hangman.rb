#!/usr/bin/env ruby
require 'io/console'

GUESSES=10
guesses_left = GUESSES
@@guessed = Array.new
# Set @@arg0 as command line argument
if ARGV.size < 1
    puts "Enter word size as command line argument."
    exit 1
elsif ARGV.size > 1
    puts "Too many arguments!"
    exit 2
else
    @@arg0=ARGV[0].to_i
end
file = File.open("./words", "r")
@@dictionary = file.read.split("\n")
file.close

def isInWord letter
if @@dictionary.class == String
    if /#{letter}/ =~ @@dictionary
        return true
    end
elsif @@dictionary.map{|word| if word !~ /.*#{letter}.*/
            word
        end
    }.compact.empty?
    return true
else
    return false
end
end

def progress
    progress_meter = ""
    if @@dictionary.class == String
        @@dictionary.split("").each do |letter|
            if !@@guessed.grep(/#{letter}/).empty?
                progress_meter+=letter
                print letter
            else
                print "_"
            end
        end
        puts ""
    else
        puts "_" * @@arg0
    end
    if progress_meter == @@dictionary
        puts "YOU WIN!"
        exit
    end
end

def eliminate guess
if @@dictionary.class != String
    if @@dictionary.map{|word| if word !~ /.*#{guess}.*/
            word
        end
    }.compact.empty?
        @@dictionary = @@dictionary[0]
#        puts @@dictionary
        puts "Fair Game..."
    else
        @@dictionary.map!{|word| if word !~ /.*#{guess}.*/
            word
        end
        }.compact!
#        puts @@dictionary
#        puts guess
    end
end
end

if @@arg0<5
    @@arg0=5
end
if @@arg0>20
    @@arg0=20
end

@@dictionary.map!{|word| if word.size == @@arg0
        word
    end }.compact!

begin
    print "Guesses: "
    @@guessed.each do |letter|
        print letter
    end
    puts " Guesses Left: " + guesses_left.to_s
    progress
    guess = STDIN.getch.downcase
    if guess !~ /[[:alpha:]]/
        puts "That is not a letter of the alphabet."
    else
        if
            @@guessed.grep(/#{guess}/)[0]==guess
            puts "You already guessed #{guess}"
        else
            @@guessed.push(guess)
            if !isInWord guess
                guesses_left -= 1
            end
            eliminate guess
        end
    end
end until guesses_left==0
if @@dictionary.class == String
    puts "YOU LOSE! The word was: " + @@dictionary
else
    puts "YOU LOSE! The word was: " + @@dictionary[0]
end
